import React from "react";
import { Routes, Route} from "react-router-dom";
import MainPage from "./pages/MainPage";
import AboutPage from "./pages/AboutPage";
import AboutNFTPage from "./pages/AboutNFTPage";
import RoadMapPage from "./pages/RoadMapPage";
import ContactsPage from "./pages/ContactsPage";
import ShopPage from "./pages/ShopPage";
import GoalPage from "./pages/GoalPage";
import NotFoundPage from "./pages/NotFoundPage";

function Routess() {
    return (
        <Routes>
            <Route path="/" element={<MainPage/>}/>
            <Route path="/about_us" element={<AboutPage/>}/>
            <Route path="/nft" element={<AboutNFTPage/>}/>
            <Route path="/roadmap" element={<RoadMapPage/>}/>
            <Route path="/goal" element={<GoalPage/>}/>
            <Route path="/contacts" element={<ContactsPage/>}/>
            <Route path="/shop" element={<ShopPage/>}/>
            <Route path ="*" element={<NotFoundPage/>}/>
        </Routes>
    )
}

export default Routess;