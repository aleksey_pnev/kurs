import React from 'react';
import Video from '../components/VideoAbout';
import Header from '../components/Header';
import Intro from '../components/Intro';
import Invis from '../components/Invisible';



const AboutPage = () => {
    return (
        <div>
            <Header/>
            <Video/>
            <Intro/>
            <Invis/>
        </div>
    );
};

export default AboutPage;
