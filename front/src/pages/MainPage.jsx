import React from 'react';
import Video from '../components/Video';
import Header from '../components/Header';


const MainPage = () => {
    return (
        <div>
            <Header/>
            <Video/>
        </div>
    );
};

export default MainPage;
