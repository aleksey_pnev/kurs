import React from 'react';
import Video from '../components/VideoAboutNFT';
import Header from '../components/Header';
import Prints from '../components/Prints';



const AboutNFTPage = () => {
    return (
        <div>
            <Header/>
            <Video/>
            <Prints/>
        </div>
    );
};

export default AboutNFTPage;
