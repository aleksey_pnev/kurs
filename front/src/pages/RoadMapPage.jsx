import React from 'react';
import Video from '../components/VideoAboutNFT';
import Header from '../components/Header';
import RoadMap from '../components/Roadmap';


const RoadMapPage = () => {
    return (
        <div>
            <Header/>
            <RoadMap/>
            <Video/>
        </div>
    );
};

export default RoadMapPage;
