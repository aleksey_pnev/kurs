import React from 'react';
import Video from '../components/VideoShop';
import Header from '../components/Header';
import Api from '../components/API';


const ShopPage = () => {
    return (
        <div>
            <Header/>            
                <Api/>
            <Video/>
        </div>
    );
};

export default ShopPage;
