import React from 'react';
import Video from '../components/VideoUnic';
import Header from '../components/Header';
import Contacts from '../components/Contacts';


const ContactsPage = () => {
    return (
        <div>
            <Header/>
            <Contacts/>
            <Video/>
        </div>
    );
};

export default ContactsPage;
