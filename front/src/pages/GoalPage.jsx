import React from 'react';
import Video from '../components/VideoUnic';
import Header from '../components/Header';
import GoalText from '../components/GoalText';


const GoalPage = () => {
    return (
        <div>
            <Header/>
            <Video/>
            <GoalText/>
        </div>
    );
};

export default GoalPage;
