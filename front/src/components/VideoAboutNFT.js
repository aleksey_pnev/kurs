import main from '../videos/aboutnft.mp4';
import styled from '@emotion/styled';

const Videos = styled.video`
  opacity: 1;
  position: fixed;
  left: 0;
  top: 0;
  bottom: 10vh;
  right: 0;
  width: 100vw;
  z-index: -9999;
`

const Back = styled.div`
  opacity: 1;
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  width: auto;
  height: auto;
  background: #262626;
  z-index: -100000;
`

const Video = () => {
        return (
            <>
            <Back/>
            <Videos autoPlay loop muted>
                <source src={main} type='video/mp4' />
            </Videos>
            </>
        )
    }
export default Video