import React from 'react';
import Header from './Header';
import Sidebar from "./Sidebar";

function Layout(props) {
    return (
        <div>
            <div>
                <Header/>
                <Sidebar/>
            </div>
        </div>
    );
}

export default Layout;