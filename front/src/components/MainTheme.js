import styled from "@emotion/styled"

export const Main = styled.div`
  height: 100vh;
  overflow: auto;
  font-family: Arial;
  &::-webkit-scrollbar {
  width: 4px;               /* ширина scrollbar */
  }
  &::-webkit-scrollbar-track {
  background: #730202;        /* цвет дорожки */
  }
  &::-webkit-scrollbar-thumb {
  background-color: black;    /* цвет плашки */
  border-radius: 10px;       /* закругления плашки */
  border: 3px solid black;  /* padding вокруг плашки */
  }
`