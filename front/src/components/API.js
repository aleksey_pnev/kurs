import React, { useEffect, useState } from 'react';
import axios from 'axios';
import JsonData from './JSONData';
import OnLoadingUserData from './Loading'
import styled from '@emotion/styled';

const Apifirst = styled.div`
  color: white;
`;

function Api() {

    const DataLoading =  OnLoadingUserData(JsonData);
  
    const [appState, setAppState] = useState(
      {
        loading: false,
        persons: null,
      }
    )
  
   useEffect(() => {
      setAppState({loading: true})
      const apiUrl = 'http://127.0.0.1:8000/api/user/';
      axios.get(apiUrl).then((resp) => {
        const user = resp.data;
        console.log(user);  
        setAppState({
        loading: false,
        persons: user
         });
      });
    }, [setAppState]);
  
  
    return (
      <div>
        <Apifirst>
          <DataLoading style ={{color: 'white'}} isLoading={appState.loading} persons={appState.persons} />
          </Apifirst>
      </div>
    );
  }
  
  export default Api;