import styled from "@emotion/styled";
import { useState } from "react";

const IntroStyle = styled.div`
  margin: 20vh 0 0 10vw;
  width: 20vw;
`

const Text = styled.button`
  margin: 10px;
  color: #F2F2F2;
  font-size: 3em;
  background: none;
  border: none;
  transition: 0.5s;
  text-shadow: 0.3em 0.4em 2px #402525;
  &:hover{
      cursor: pointer;
  }
`


const Description = styled.div`
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 20vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`



const Intro = () => { 
    let [onPrints, setOnPrints] = useState(false);
    let [onCloth, setOnCloth] = useState(false);
    let [onCut, setCut] = useState(false);
    let [onPrint, setOnPrint] = useState(false);

    return( // sdelat` cherez spisok kak sidebar izmenenie tsveta
        <>
            <IntroStyle>
            <div>
                <Text onClick={() => setOnPrints(!onPrints)}>  
                    Принты
                </Text>
                {onPrints && 
                <Description >
                    Уникальные и неповторимые принты, которые рисуются опытными художниками специально для вашего образа.
                </Description>
                }
                
            </div>
            <div>
                <Text onClick={() => setOnCloth(!onCloth)}>     
                    Ткань
                </Text>
                {onCloth && 
                <Description>
                    В качестве материала худаков мы берем только качественную ткань из натурально хлопка. Которая не скатывается, не облазит и просто не теряет вид.
                </Description>
                }

            </div>
            <div>
                <Text onClick={() => setCut(!onCut)}> 
                    Крой
                </Text>
                {onCut && 
                <Description>
                    Вся одежда «Rage Raven» отшивается в России умелыми мастерами с многолетним опытом. Данный крой был выведен из 10 других кроев, имеющих свои недостатки. Это самый оптимальный, удобный и стилевый крой. Честно)
                </Description>
                }
            </div>
            <div>
                <Text onClick={() => setOnPrint(!onPrint)}> 
                    Печать
                </Text>
                {onPrint && 
                <Description>
                    Все принты из коллекции «БЕСкультурье» сделаны методом цифровой печати. Этот метод позволяет передать все цвета яркими и красочными. Также, принт обладает устойчивостью к износу и выцветанию, после первой стирки краски не слезут. Да и зачем вам стирать принт, он же съемный.
                </Description>
                }
            </div>
            </IntroStyle>
        </>
    )
}

export default Intro