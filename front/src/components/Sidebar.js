import React from "react";
import styled from '@emotion/styled'
import { Link } from "react-router-dom";

const SidebarParent = styled.div`
position: fixed;
top: 10vh;
right: 0;
margin-top: 2px;
  background: #262626;
  a {
    text-decoration: none;
  }
  width: 10%;
  & > div {
    width: 10%;
    height: 100vh;
  }
`;

const SidebarItem = styled.div`
  padding: 4px 12px;
  transition: all 0.25s ease-in-out;
  //Change the background color if 'active' prop is received
  background: ${props => props.active ? "#b15b00" : ""};
  margin: 4px 12px;
  border-radius: 4px;

  p {
    color: white;
    font-weight: bold;
    text-decoration: none;
  }
  p:hover{
      color: #BF1304;
      transition: all 0.15s ease-in-out;
    cursor:pointer;}

`;

const SidebarItems = [
    {
        name: "Главная",
        route: '/'
    },
    {
        name: "О⠀нас",
        route: '/about_us',
    },
    {
        name: "NFT⠀Токены",
        route: '/nft'
    },
    {
        name: "RoadMap",
        route: '/roadmap'
    },
    {
        name: "Наша⠀цель",
        route: '/goal'
    },
    {
        name: "Контакты",
        route: '/contacts'
    },
    {
        name: "Пользователи",
        route: '/shop'
    },
]; 


function Sidebar() {
    return (
        <>
            <SidebarParent>
                <div>
                {
                    SidebarItems.map((item,index)=> {
                        return (
                                <SidebarItem key={item.name}>
                                    <Link to={item.route}>
                                        <p>{item.name}</p>
                                    </Link>
                                </SidebarItem>
                        );
                    })
                }
                </div>
            </SidebarParent>
        </>
    );
}

export default Sidebar;