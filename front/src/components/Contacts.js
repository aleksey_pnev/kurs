import vk from '../pictures/vk.png'
import logo_black from '../pictures/logo_black.png'
import twitter from '../pictures/twitter.png'
import instagram from '../pictures/instagram.png'
import styled from '@emotion/styled'

const ContactsStyle = styled.div`
  margin: 15vh 0 0 5vw;
`

const Logo = styled.img`
  margin: 5px;
  margin: 1vh 0 0 55px;
`
const Vk = styled.img`
  margin-left: 20px;
  margin-top: 10px;

`

const Inst = styled.img`
  margin-left: 20px;
  margin-top: 10px;

`

const Twitter = styled.img`
  margin-left: 20px;
  margin-top: 10px;

`

const Text = styled.div`
  margin: 1vh 0 0 70px;
  color: #F2F2F2;
  font-size: 1.1em;
`

const Text2 = styled.div`
display: inline-block;
  margin-left: 10px;
  margin-top: 50px;
  color: #F2F2F2;
  font-size: 1em;
`

const Border = styled.div`
  border: solid #F23005 3px;
  display: inline-block;
  padding: 5px;
  padding-right: 10px;
`

const Contacts = () => {
    return(
        <>
        <ContactsStyle>
            <Border>
            <Logo
                src={logo_black} width="100" alt="raven_black">
            </Logo>
            <Text>
                Контакты
            </Text>
            <Text2>
                rageraven2021@gmail.com 
            </Text2>
            <div>
            <a href="https://vk.com/rage_raven" target="blank" >
                <Vk
                    src={vk} width="50" alt="vk">
                </Vk>
            </a>
            <a href="http://localhost:3000/notwitter" target="blank" >
                <Twitter
                    src={twitter} width="50" alt="twitter">
                </Twitter>
            </a>
            <a href="http://localhost:3000/noinst" target="blank" >
                <Inst
                    src={instagram} width="50" alt="twitter">
                </Inst>
            </a>
            </div>
            </Border>
        </ContactsStyle>
        </>
    )
}

export default Contacts;