import angel3 from '../pictures/prints/angel3.jpg'
import david2 from '../pictures/prints/david2.jpg'
import notwar2 from '../pictures/prints/notwar2.jpg'
import Pompei from '../pictures/prints/Pompei.jpg'
import ruki from '../pictures/prints/ruki.jpg'
import notaste from '../pictures/prints/notaste.jpg'
import noculture from '../pictures/prints/noculture.jpg'
import meduza from '../pictures/prints/meduza.jpg'
import depravity from '../pictures/prints/depravity.jpg'
import power from '../pictures/prints/power.jpg'
import styled from '@emotion/styled'


const Main = styled.div`
    margin: 10vh 18vw;
`

const Image = styled.img`
  margin-left: 5vw;
  margin-top: 5vh;
  display: block;
  transition: 0.5s;

`

const Text = styled.div`
    margin-left: 24vw;
    font-size: 3em;
    color: #059fb0
`

const Prints = () => {
    return(
        <>
            <Main>
                <Text>
                    NFT Tokens
                </Text>
                <Image
                    src={angel3} width="900" alt="">
                </Image>
                <Image
                    src={david2} width="900" alt="">
                </Image>
                <Image
                    src={notwar2} width="900" alt="">
                </Image>
                <Image
                    src={Pompei} width="900" alt="">
                </Image>
                <Image
                    src={ruki} width="900" alt="">
                </Image>
                <Image
                    src={notaste} width="900" alt="">
                </Image>
                <Image
                    src={noculture} width="900" alt="">
                </Image>
                <Image
                    src={meduza} width="900" alt="">
                </Image>
                <Image
                    src={depravity} width="900" alt="">
                </Image>
                <Image
                    src={power} width="900" alt="">
                </Image>
            </Main>
        </>
    )
}

export default Prints;