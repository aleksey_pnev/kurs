import Loader from "./Loader";

function OnLoadingUserData(Component) {
    return function LoadingPersonsData({ isLoading, ...props }) {
        if (!isLoading) return <Component {...props} />

        else return (
            <div>
                <Loader/>
            </div>
        )
    }
}

export default OnLoadingUserData;