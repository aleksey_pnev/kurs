import './loader.css'
import styled from '@emotion/styled';

const Style = styled.div`
display: flex;
margin-top: 35vh;
justify-content: center;
align-items: : middle;
`
const Loader = () =>{
  return(
    <Style>
      <div class="lds-dual-ring"></div>
    </Style>
  )
}


export default Loader;