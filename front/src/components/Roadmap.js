import roadmap from '../pictures/Palka.png'
import styled from '@emotion/styled'
import { useState } from "react";

const RoadStyle = styled.div`
  position: relative;
  margin: 17vh 0 0 15vw;
`
const RoadStyle2 = styled.div`
  position: absolute;
`

const Roadmap = styled.img`
  margin: 1vh 0 0 5vw;
  position: absolute;
  z-index: -2;
`

const RoadmapBtn1 = styled.div`
  width: 2vw;
  height: 4vh;
  background: none;
  position: absolute;
  top: 5vh;
  left: 11vw;
  z-index: 0;
&:hover{
  cursor: pointer
}
`
const RoadmapBtn2 = styled.div`
width: 2vw;
height: 4vh;
background: none;
position: absolute;
top: 5vh;
left: 20.3vw;
z-index: 0;
&:hover{
cursor: pointer
}
`
const RoadmapBtn3 = styled.div`
width: 2vw;
height: 4vh;
background: none;
position: absolute;
top: 5vh;
left: 29.7vw;
z-index: 0;
&:hover{
cursor: pointer
}
`
const RoadmapBtn4 = styled.div`
width: 2vw;
height: 4vh;
background: none;
position: absolute;
top: 5vh;
left: 39vw;
z-index: 0;
&:hover{
cursor: pointer
}
`
const RoadmapBtn5 = styled.div`
width: 2vw;
height: 4vh;
background: none;
position: absolute;
top: 5vh;
left: 48.5vw;
z-index: 0;
&:hover{
cursor: pointer
}
`

const Description1 = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 5px;
background: #2c2c2c;
position: absolute;
top: 12vh;
left: 5vw;
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 20vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`

const Description2 = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 5px;
background: #2c2c2c;
position: absolute;
top: 12vh;
left: 16vw;
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 20vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`

const Description3 = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 5px;
background: #2c2c2c;
position: absolute;
top: 12vh;
left: 25vw;
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 20vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`

const Description4 = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 5px;
background: #2c2c2c;
position: absolute;
top: 12vh;
left: 35vw;
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 19vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`

const Description5 = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 5px;
background: #2c2c2c;
position: absolute;
top: 12vh;
left: 43vw;
  margin: 0px;
  color: #c4c4c4;
  transition: 3s;
  font-size: 1em;
  margin-left: 2vw;
  width: 20vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`

const Zagolovok = styled.span`
  color: #BF1304;
  font-size: 1.5em;
  text-shadow: 0.3em 0.2em 3px rgba(150, 18, 6, 0.5);
`


const RoadMap = () => {

  let [first, setOnfirst] = useState(false);
  let [second, setOnsecond] = useState(false);
  let [third, setOnthird] = useState(false);
  let [four, setOnfour] = useState(false);
  let [five, setOnfive] = useState(false);


    return(
        <>
        <RoadStyle>
          <RoadStyle2>
                <Roadmap
                    src={roadmap} width="800px" alt="Roadmap">
                </Roadmap>
                <div>
                <RoadmapBtn1 onClick={() => {setOnfirst(!first); setOnsecond(false); setOnthird(false); setOnfour(false); 
                setOnfive(false)}} />     
                {first && 
                <Description1>
                  <Zagolovok>
                    1-й этап
                  </Zagolovok><br/>
                    Выявление потребителей, наработка опыта.
Самым первым делом перед нами стояла задачи понять, кто наш потребитель, его вкусы к одежде и особенности. Были сделаны пробные продажи, проанализирована потребность покупателей. 
                </Description1>
                }
            </div>
            <div>
                <RoadmapBtn2 onClick={() => {setOnfirst(false); setOnsecond(!second); setOnthird(false); setOnfour(false); 
                setOnfive(false)}} />     
                {second && 
                <Description2>
                  <Zagolovok>
                    2-й этап
                  </Zagolovok><br/>
Дебютная коллекция «Бескультурье»
Долгий процесс подготовки к дебюту, отрисовка принтов, создание паспортов к худаку на основе NFT токена, который можно продать. И главное - новый продукт, который отличается тем, что картинку на вещи не обязательно носить вечно, её можно и менять. Съемные картинки со смыслом и оригинальным дизайном будут отличительной особенностью с любыми другими худаками.                
</Description2>
                }
            </div>
            <div>
                <RoadmapBtn3 onClick={() => {setOnfirst(false); setOnsecond(false); setOnthird(!third); setOnfour(false); 
                setOnfive(false)}} />     
                {third && 
                <Description3>
                  <Zagolovok>
                    3-й этап
                  </Zagolovok><br/>
Создание коллекций с наиболее уникальными принтами, связанными с NFT токеном. Повышение спроса на NFT и тем самым повышение его стоимости.
                </Description3>
                }
            </div>
            <div>
                <RoadmapBtn4 onClick={() => {setOnfirst(false); setOnsecond(false); setOnthird(false); setOnfour(!four); 
                setOnfive(false)}} />     
                {four && 
                <Description4>
                  <Zagolovok>
                    4-й этап
                  </Zagolovok><br/>
Создание коллекций на основе коллобораций с другими художниками. С каждой новой коллекцией ценность самых первых и раритетных NFT возрастает.
                </Description4>
                }
            </div>
            <div>
                <RoadmapBtn5 onClick={() => {setOnfirst(false); setOnsecond(false); setOnthird(false); setOnfour(false); 
                setOnfive(!five)}} />     
                {five && 
                <Description5>
                  <Zagolovok>
                    5-й этап
                  </Zagolovok><br/>
Создание полностью уникальных вещей конечным потребителем с привязкой к NFT.
                </Description5>
                }
            </div>
            </RoadStyle2>
        </RoadStyle>
        </>
    )
}

export default RoadMap;