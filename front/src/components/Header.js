import styled from "@emotion/styled"
import { GiHamburgerMenu } from 'react-icons/gi';
import logo_white from '../pictures/logo_white.png';
import Sidebar from "./Sidebar";
import { useState } from "react";


const HeaderStyle = styled.div`
  background-color: #262626;
  height: 10vh;
  weight: 100vw;
  margin: 0px;
  top: 0;
  position: sticky;
  border-bottom: 2px solid #730202;
`

const BurgerIcon = styled.button`
  color: #BF1304;
  font-size: 3em;
  display: inline-block;
  position: relative;
  margin: 2vh 2vh 2vh 95%;
  background: none;
  border: 0;
  &:hover{
    cursor: pointer;
  }
`

const RavenIcon = styled.img`
  position: fixed;
  margin-left: 1vw;
  margin-top: 1vh;
`

const Header = () => {

    const [isShown, setIsShown] = useState(true);
  
    const toggleFIeldset = () => setIsShown(!isShown);

  
    return(
        <>
            <HeaderStyle>
              <RavenIcon
                  src={logo_white} width="60" alt="raven">
              </RavenIcon>
                <BurgerIcon>
                    <GiHamburgerMenu  onClick={toggleFIeldset}/>
                </BurgerIcon>
              {isShown &&
                <Sidebar/>
              }
            </HeaderStyle>
        </>
    )
}

export default Header;