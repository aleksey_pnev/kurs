import styled from '@emotion/styled'

const Description = styled.div`
border: solid #F2F2F2 3px;
border-radius: 8px;
padding: 1vh;
background: #2c2c2c;
  margin: 5vh 3vw ;
  color: #f0f0f0;
  font-size: 1em;
  margin-left: 2vw;
  width: 15vw;
  text-shadow: 0.3em 0.4em 2px #361818;
`


function JsonData(props) {

    const { persons } = props

    if (!persons || persons.length === 0) return <p>Нет данных.</p>
    else
    return (
        <div>
                    {
                        persons.map((person) =>
                        <Description>
                        <div key={person.id}>
                                <p>Имя: {person.first_name}</p>
                                <p>Фамилия: {person.last_name}</p>
                                <p>Адрес: {person.address}</p>
                                <p>Телефон: {person.phone_number}</p>
                        </div>
                        </Description>
                        )
                    }
      </div>
    )
}

export default JsonData