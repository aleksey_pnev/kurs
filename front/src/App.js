import { Main } from "./components/MainTheme";
import Routess from "./Routes";

const  App = () => {
  return (
    <>
      <Main>
        <Routess/>
      </Main>
    </>
  );
}

export default App;
