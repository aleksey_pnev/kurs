## Backend

-   `cd back`
-   `python -m venv env`
-   (on Windows) `.\env\Scripts\activate`
-   `pip install -r requirements.txt`
-   `cd backend`
-   `python manage.py migrate`
-   `python manage.py runserver`

## Как с этим работать?

C API можно работать через GET, POST, PUT, PATCH и DELETE запросы.

Например, чтобы отправить запрос на создание пользователя, нам необходимо отослать POST на `users/`, со всеми требуемыми полями

Чтобы изменить токены в корзине, достаточно лишь отправить PATCH запрос на `cart/<id корзины>/`, в поле `nfts` указав нужные id в виде массива
