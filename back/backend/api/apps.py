from django.apps import AppConfig


class ApiConfig(AppConfig):
    # auto-incrementing field for any models
    default_auto_field = 'django.db.models.BigAutoField'
    # app name to register
    name = 'api'
