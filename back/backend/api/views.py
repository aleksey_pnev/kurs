from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from . import models, serializers

# Create your views here.


#  all views basically do the same thing: get all objects using serializers
class TokenView(viewsets.ModelViewSet):
    queryset = models.Token.objects.all()
    serializer_class = serializers.TokenSerializer

    # not sure if this is needed?
    parser_classes = (MultiPartParser, FormParser)


class PictureView(viewsets.ModelViewSet):
    queryset = models.Picture.objects.all()
    serializer_class = serializers.PictureSerializer


class VideoView(viewsets.ModelViewSet):
    queryset = models.Video.objects.all()
    serializer_class = serializers.VideoSerializer


class UserView(viewsets.ModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer


class UserBanView(viewsets.ModelViewSet):
    queryset = models.UserBan.objects.all()
    serializer_class = serializers.UserBanSerializer


class CartView(viewsets.ModelViewSet):
    queryset = models.Cart.objects.all()
    serializer_class = serializers.CartSerializer
