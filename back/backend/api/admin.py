from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Picture)
admin.site.register(models.Video)
admin.site.register(models.Token)
admin.site.register(models.Cart)
admin.site.register(models.User)
admin.site.register(models.UserBan)
# FIXME: registering all the models by hand? there's gotta be a better way to do this!
