from django.urls import include, path
from rest_framework import routers

from . import views

# get a router object, then register all the urls based on views
router = routers.DefaultRouter()
router.register('token', views.TokenView)
router.register('user', views.UserView)
router.register('user_ban', views.UserBanView)
router.register('cart', views.CartView)
router.register('picture', views.PictureView)
router.register('video', views.VideoView)

urlpatterns = [
    path('', include(router.urls))
]
