# Generated by Django 4.0.3 on 2022-05-20 13:15

import api.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_picture_image_alter_picture_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='image',
            field=models.ImageField(upload_to=api.models.Picture.upload_to),
        ),
        migrations.AlterField(
            model_name='picture',
            name='token',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pictures', to='api.token'),
        ),
    ]
