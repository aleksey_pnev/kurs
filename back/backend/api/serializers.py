from rest_framework import serializers

from . import models


class PictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Picture
        fields = (
            'id',
            'token',
            'image',
        )


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Video
        fields = '__all__'


class TokenSerializer(serializers.ModelSerializer):
    pictures = PictureSerializer(many=True, read_only=True)
    # video = serializers.PrimaryKeyRelatedField(
    #     many=False, queryset=models.Video.objects.all(), allow_null=True)

    class Meta:
        model = models.Token
        fields = (
            'name',
            'description',
            'opensea_link',
            'pictures',
            # 'video',
        )


class UserSerializer(serializers.ModelSerializer):
    user_ban = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    cart = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = models.User
        fields = (
            'first_name',
            'last_name',
            'patronymic',
            'address',
            'phone_number',
            'cart',
            'user_ban',
        )


class UserBanSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = models.UserBan
        fields = (
            'user',
            'date_received',
            'date_expiring',
            'reason',
        )


class CartSerializer(serializers.ModelSerializer):
    nfts = serializers.PrimaryKeyRelatedField(
        required=False, many=True, queryset=models.Token.objects.all()
    )

    class Meta:
        model = models.Cart
        fields = (
            'nfts',
            'user',
        )
