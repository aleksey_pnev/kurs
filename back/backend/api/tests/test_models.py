from django.test import TestCase
from .. import models


class UserTest(TestCase):
    """Test module for User model"""

    def setUp(self):
        models.User.objects.create(
            first_name='John',
            last_name='Code',
            patronymic='Voximovich',
            address='St. ABC',
            phone_number='612345',
        )
        models.User.objects.create(
            first_name='Ivan',
            last_name='Dude',
            patronymic='Kanazovich',
            address='BRLP',
            phone_number='3331221',
        )

    def test_users(self):
        user_john = models.User.objects.get(first_name='John')
        user_ivan = models.User.objects.get(first_name='Ivan')

        self.assertEqual(str(user_john), 'John Code Voximovich')
        self.assertEqual(str(user_ivan), 'Ivan Dude Kanazovich')
