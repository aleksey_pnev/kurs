import json
import shutil
from io import BytesIO

from django.test import override_settings
from PIL import Image
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIRequestFactory, APITestCase

from .. import models

# TODO: add default methods to create objects

TEST_MEDIA_DIR = 'test_data'


class UserTestCase(APITestCase):
    def setUp(self):
        self.data = {
            'first_name': 'John',
            'last_name': 'Code',
            'patronymic': 'Voximovich',
            'address': 'St. ABC',
            'phone_number': '612345',
        }

        self.data_patch = {
            'first_name': 'Santa',
        }

        self.data2 = {
            'first_name': 'Nick',
            'last_name': 'Bro',
            'patronymic': 'Xamani',
            'address': 'St. EDF',
            'phone_number': '666666',
        }
        self.user_creation(self.data)

    def user_creation(self, data):
        self.post_response = self.client.post('/api/user/', data)

    def test_user_post(self):
        self.assertEqual(self.post_response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(models.Cart.objects.count(), 1)
        self.assertEqual(models.User.objects.count(), 1)
        self.assertEqual(models.User.objects.get().first_name,
                         self.data['first_name'])

    def test_user_patch(self):
        response = self.client.patch('/api/user/1/', self.data_patch)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.User.objects.get().first_name,
                         self.data_patch['first_name'])

    def test_user_put(self):
        response = self.client.put('/api/user/1/', self.data2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.User.objects.get().first_name,
                         self.data2['first_name'])

    def test_user_delete(self):
        response = self.client.delete('/api/user/1/')
        self.assertEqual(response.status_code,
                         status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.User.objects.count(), 0)
        self.assertEqual(models.Cart.objects.count(), 0)


class TokenTestCase(APITestCase):
    def setUp(self):
        self.data = {
            "name": "Useful",
            "description": "Money",
            "opensea_link": "example.com",
        }

        self.data_patch = {
            'name': 'Useless',
        }

        self.data2 = {
            "name": "Useful",
            "description": "Money",
            "opensea_link": "example.com",
        }
        self.token_creation(self.data)

    def token_creation(self, data):
        self.post_response = self.client.post('/api/token/', data)

    def test_token_post(self):
        self.assertEqual(self.post_response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(models.Token.objects.count(), 1)
        self.assertEqual(models.Token.objects.get().name, self.data['name'])

    def test_token_patch(self):
        response = self.client.patch('/api/token/1/', self.data_patch)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Token.objects.get().name,
                         self.data_patch['name'])

    def test_token_put(self):
        response = self.client.put('/api/token/1/', self.data2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Token.objects.get().name,
                         self.data2['name'])

    def test_token_delete(self):
        response = self.client.delete('/api/token/1/')
        self.assertEqual(response.status_code,
                         status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.Token.objects.count(), 0)


class PictureTestCase(APITestCase):
    # this test is so hacked together, but it works
    def setUp(self):
        self.normal_token = models.Token.objects.create(
            name='Token name',
            description='Token description',
            opensea_link='example.com',
        )
        self.normal_token.save()
        self.url = reverse('picture-list')

    def tearDown(self):
        try:
            shutil.rmtree(TEST_MEDIA_DIR)
        except OSError:
            print('did\'t find test image directory')

    def generate_photo_file(self):
        file = BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)
        return file

    def test_upload_not_picture(self):
        data = {
            'token': self.normal_token.id,
            'image': 'not an image, clearly',
        }
        response = self.client.post(self.url, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @override_settings(MEDIA_ROOT=(TEST_MEDIA_DIR + '/media'))
    def test_upload_picture(self):
        self.photo_file = self.generate_photo_file()
        data = {
            'token': self.normal_token.id,
            'image': self.photo_file,
        }

        response = self.client.post(self.url, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
