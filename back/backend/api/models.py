import os

from django.db import models
from django.utils.text import slugify
from datetime import datetime

# Create your models here.


class Token(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    opensea_link = models.CharField(max_length=200)

    class Meta:
        ordering = ('name',)

    def __str__(self) -> str:
        return self.name


class Picture(models.Model):
    def upload_to(instance, filename):
        """
            Slugifies image name and attaches current date 
        """
        name, ext = os.path.splitext(filename)
        return 'images/' + slugify(f'{name}_{datetime.now().isoformat()}') + ext

    token = models.ForeignKey(
        Token,
        on_delete=models.CASCADE,
        related_name='pictures',
        blank=True,
        null=True,
    )
    image = models.ImageField(upload_to=upload_to)
    # thumbnail = models.ImageField(upload_to='uploads/', blank=True, null=True)

    # def get_thumbnail(self):
    #     if self.thumbnail:
    #         return f'/{self.thumbnail.url}/'
    #     if self.image:
    #         self.thumbnail = self.make_thumbnail(self.image)
    #         self.save()
    #         return
    #     return ''
    # TODO: implement make_thumbnail


class Video(models.Model):
    token = models.OneToOneField(
        Token, on_delete=models.CASCADE, related_name='video')
    # TODO: add hash field for videos


class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    patronymic = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=50)

    # create Cart on User creation
    def save(self, *args, **kwargs):
        created = not self.pk
        super().save(*args, **kwargs)
        if created:
            Cart.objects.create(user=self)

    # better display in admin panel
    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name} {self.patronymic}'


class UserBan(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='user_ban')
    # date recieved automatically assumes that user got banned whenever we created a new UserBan in database
    date_received = models.DateTimeField(auto_now_add=True)
    date_expiring = models.DateTimeField()
    reason = models.CharField(max_length=50)

    class Meta:
        ordering = ('-date_received',)


class Cart(models.Model):
    # cart can't exist without the user
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='cart')
    # but our cart can exist without anything in it!
    nfts = models.ManyToManyField(Token, blank=True, related_name='nfts')
    # TODO: raise some exception when Cart's created on its own
